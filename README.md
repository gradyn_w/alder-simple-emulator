# Luna Simple Emulator
This is a virtual Luna Simple panel capable of connecting to a real instance of SecureNet, or run locally for Tech Support. It supports multiple cloud or local instances. You can create virtual sensors and connect them to the panel and even sync the virtual sensors with SecureNet
## Setup
For most use cases, you probably want to use `panel.html`. This contains the emulator itself, the control overlay, and an interactive graphic of the panel. Note that many features wont work if you have CORS setup improperly.
In `panel.html`, there are iFrames of `index.html` and `emulator-control/index.html`. If the iFrames cannot access the parent (`panel.html`), many features wont work.
For testing/development purposes, the easiest way to run the emulator is with a localhost-only web server that allows Cross-Origin Resource Sharing (CORS). The easiest way to do this is with the included `testserver.py` web server.
### Setup Development Environment using testserver.py
Make sure you have a modern version of python 3 installed. Next, install `Flask`
```bash
pip install Flask
```
now from inside the base directory of the project, you can start the development server
```
python testserver.py
```
If all went well, you can now access the emulator at `http://127.0.0.1:5000/panel.html`