// Set sensors in application memory to a blank dictionary if there are no sensors in LocalStorage. Otherwise, load them from LocalStorage
if (typeof localStorage.instanceID == "undefined") { // Unmanaged instance
    if (typeof localStorage.sensors == "undefined") {
        sensors = {}
    } else {
        sensors = JSON.parse(localStorage.sensors)
    }
} else { // Managed instance
    let localInstances = JSON.parse(localStorage.localInstances)
    sensors = localInstances[localStorage.instanceID].sensors
}
/**
 * Adds a new sensor with a randomly generated TXID and the given name to the panel
 */
function newSensor () {
    sensor = {
        "nickname": document.getElementById("new").value,
        "state": false,
        "tamper": false,
        "low-bat": false,
        "supervised": true
    }
    txid = ""
    for (i = 0; i<=5; i++) {
        txid = txid + Math.floor(Math.random() * 10)
    }
    sensors[txid] = sensor
    updateStorage()
    buildSensorTable()
    document.getElementById("new").value = ""
}
/**
 * Generates an interactive table of sensors in the overlay
 */
function buildSensorTable () {
    let temp_sensors = Array.from(document.getElementsByClassName("sensor"))
    for (node in temp_sensors) { // Clear the table
        temp_sensors[node].remove()
    }
    for (sensor in sensors) {
        row = document.createElement("tr")
        // Name
        column = document.createElement("td")
        column.innerHTML = "<input value='" + sensors[sensor]["nickname"] + "'>"
        row.appendChild(column)
        // TXID
        column = document.createElement("td")
        column.innerText = sensor
        row.appendChild(column)
        // State
        column = document.createElement("td")
        if (sensors[sensor]["state"]) {
            column.innerHTML = "<input type='checkbox' checked>"
        } else {
            column.innerHTML = "<input type='checkbox'>"
        }
        row.appendChild(column)
        // Tampered
        column = document.createElement("td")
        if (sensors[sensor].tamper) {
            column.innerHTML = "<input type='checkbox' checked>"
        } else {
            column.innerHTML = "<input type='checkbox'>"
        }
        row.appendChild(column)
        // Low Battery
        column = document.createElement("td")
        if (sensors[sensor]["low-bat"]) {
            column.innerHTML = "<input type='checkbox' checked>"
        } else {
            column.innerHTML = "<input type='checkbox'>"
        }
        row.appendChild(column)
        // Supervised
        column = document.createElement("td")
        if (sensors[sensor]["supervised"]) {
            column.innerHTML = "<input type='checkbox' checked>"
        } else {
            column.innerHTML = "<input type='checkbox'>"
        }
        row.appendChild(column)
        // Manual Control
        column = document.createElement("td")
        column.innerHTML = "<button>Event packet</button><button>Supervision packet</button>"
        row.appendChild(column)
        // Delete
        column = document.createElement("td")
        column.innerHTML = "<button onclick='deleteSensor(this)'>Delete</button>"
        row.appendChild(column)
        // Done
        row.classList.add("sensor")
        row.id = sensor
        document.getElementById("sensor-table").prepend(row)
    }
    inputs = document.getElementById("sensor-table").getElementsByTagName("input")
    for (input of inputs) { // for some reason there isnt 1 universal event for "input updated"
        if (input.type == "text") {
            input.oninput = updateSensors
        }
        if (input.type == "checkbox") {
            input.onchange = updateSensors
        }
    }
}
deleteSensor = function (node) {
    let sensor = node.parentNode.parentNode.id // get the ID of the sensor
    delete sensors[sensor]
    updateStorage()
    buildSensorTable()
}
/**
 * Save sensor list in application memory to LocalStorage. You usually don't want to call this directly, use update_sensors() instead
 */
function updateStorage () {
    if (typeof localStorage.instanceID == "undefined") { // Unmanaged instance
        localStorage.sensors = JSON.stringify(sensors)
    } else { // Managed instance
        parent.document.getElementById("alder-simple").contentWindow.syncManagedInstance()
    }
}
/**
 * Updates all sensors both in system memory and LocalStorage based on all the information entered into the table
 */
function updateSensors () {
    updated_sensors = {}
    for (sensor in sensors) {
        sensor_node = document.getElementById(sensor) 
        updated_sensors[sensor] = {
            "nickname": sensor_node.children[0].firstChild.value,
            "state": sensor_node.children[2].firstChild.checked,
            "tamper": sensor_node.children[3].firstChild.checked,
            "low-bat": sensor_node.children[4].firstChild.checked,
            "supervised": sensor_node.children[5].firstChild.checked
        }
    }
    sensors = updated_sensors
    updateStorage()
}
window.onload = function () {
    buildSensorTable()
    document.getElementById("new").onkeyup = function (key) { // If enter is pressed in the new sensor name, create the sensor
        if (key.key == "Enter") {
            newSensor()
        }
    }
}
/**
 * Switches tabs
 * @param {string} id ID of tab to switch to
 * @param {Object} tab HTML element representing the tab button
 */
function setTab(id, tab) {
    for (let node of document.getElementsByClassName("tab")) {
        node.style.display = "none"
    }
    for (let node of document.getElementsByClassName("selected")) {
        node.classList.remove("selected")
    }
    tab.classList.add("selected")
    document.getElementById(id).style.display = "flex"
}