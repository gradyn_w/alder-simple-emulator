"use strict"
var localInstances = {}
function loadLocalInstances () {
    if (typeof localStorage.localInstances == "undefined") {
        localInstances = {}
    } else {
        localInstances = JSON.parse(localStorage.localInstances)
    }
}
window.onload = function () {
    generateTable("local", true)
    document.getElementById("new-nickname").onkeyup = function (key) {
        if (key.key == "Enter") {
            newInstance("local") // TODO: Make this work in all scopes
        }
    }
}
loadLocalInstances()
/**
 * Populates the table with a list of instances
 * @param {"local" | "private" | "public"} scope which group of instances to display
 * @param {boolen} allowNew If creating new instances should be enabled or not. false by default. Generally, this should be true for local and private instances, and false for public instances
 */
function generateTable(scope, allowNew=false) {
    let tempNodes = Array.from(document.getElementsByTagName("tbody")[1].getElementsByTagName("tr"))
    for (let node of tempNodes) { // Nuke the table
        node.remove()
    }
    if (scope == "local") {
        for (const instance in localInstances) {
            document.getElementsByTagName("tbody")[1].appendChild(generateRow(localInstances[instance]))
        }
    }
    if (allowNew) {
        let row = document.createElement("tr")
        let column = document.createElement("td")
        column.innerHTML = "<input id='new-nickname' placeholder='New Instance'>"
        row.appendChild(column)
        for (let i = 0; i < 8; i++) { // The next 8 columns are blank for this
            column = document.createElement("td")
            row.appendChild(column)
        }
        column = document.createElement("td")
        column.innerHTML = "<button onclick='newInstance(\"local\")'>Create</button>" // TODO: make this work in all scopes not just local
        row.appendChild(column) 
        document.getElementsByTagName("tbody")[1].appendChild(row)
    }
}
function updateNames () {
    for (const node of document.getElementsByTagName("tr")) {
        if (node.id != "") {
            localInstances[node.id].nickname = node.firstChild.firstChild.value
            saveLocalInstances()
        }
    }
}
/**
 * Generates a table row that represents an instance
 * @param {Instance} instance Instance to create a table row for
 * @param {boolean} allowNew Rather or not creating new instances should be allowed. false by default. Generally, this should be true for local and cloud instances, but false for public instances
 * @returns {Object} an HTML table row
 */
function generateRow(instance) {
    let row = document.createElement("tr")
    row.id = instance.instanceID
    // Nickname
    let column = document.createElement("td")
    column.innerHTML = "<input oninput=updateNames() value='" + instance.nickname + "'>"
    row.appendChild(column)
    // Owner
    column = document.createElement("td")
    column.innerText = instance.owner
    row.appendChild(column)
    // state
    column = document.createElement("td")
    column.innerText = instance.globalState
    row.appendChild(column)
    // sensors
    column = document.createElement("td")
    column.innerText = Object.keys(instance.sensors).length
    row.appendChild(column)
    // panel sensors
    column = document.createElement("td")
    column.innerText = Object.keys(instance.panelSensors).length
    row.appendChild(column)
    // users
    column = document.createElement("td")
    column.innerText = Object.keys(instance.users).length - 2 // Do not include Master and Hostage
    row.append(column)
    // boot
    column = document.createElement("td")
    column.innerHTML = "<button onclick='bootInstance(localInstances[this.parentNode.parentNode.id])'>Boot</button>" // TODO: make this work in all scopes
    row.appendChild(column)
    // reassign
    column = document.createElement("td")
    column.innerHTML = "<button>Reassign</button>"
    row.appendChild(column)
    // clone
    row.appendChild(column)
    column = document.createElement("td")
    column.innerHTML = "<button>Clone</button>"
    row.appendChild(column)
    // delete
    row.appendChild(column)
    column = document.createElement("td")
    column.innerHTML = "<button onclick='deleteInstance(this.parentNode.parentNode.id, \"local\")'>Delete</button>" // TODO: make this work in all scopes
    row.appendChild(column)
    return row
}
/**
 * Generates a new instance from the "new instance" row
 * @param {"local" | "private" | "public"} scope Which scope the instance will be created in
 * @returns {Instance} the new instance that was just created
 */
function newInstance (scope) {
    let instance = {}
    let instanceID = ""
    for (let i = 0; i<=5; i++) {
        instanceID = instanceID + Math.floor(Math.random() * 10)
    }
    instance.instanceID = instanceID
    instance.globalState = "disarmed"
    instance.nickname = document.getElementById("new-nickname").value
    instance.sensors = {}
    instance.panelSensors = {}
    instance.settings = { // Default Settings
        "led_buttons": true,
        "panel-mount": "wall", // Can be: wall, table
        "secure-arm": false,
        "exit-delay": 60, // can be: 45, 60, 75, 90, 105, 120, 135, 150, 165, 180, 195, 210, 225, 240, 255
        "transmission-delay": 0 // can be: 0, 15, 30, 45
    }
    instance.users = {
        "master": 1234,
        "hostage": null
    }
    if (scope == "local") {
        localInstances[instanceID] = instance
    }
    saveLocalInstances()
    generateTable(scope, true)
    return instance
}
/**
 * Commits local instances to application storage
 */
function saveLocalInstances () {
    localStorage.localInstances = JSON.stringify(localInstances)
}
/**
 * Deletes an instance
 * @param {Instance} instance The instance to delete
 * @param {"local" | "private" | "public"} scope What scope the Instance is a member of
 */
function deleteInstance(instance, scope) {
    if (scope == "local") {
        delete localInstances[instance]
        generateTable(scope, true) // TODO: false if we are in the public scope
        saveLocalInstances()
    }
}
function bootInstance(instance) {
    localStorage.instanceID = instance.instanceID
    // Boot
    location.pathname = "panel.html"
}