"use strict"
/**
 * Switches display to a given screen
 * @param {string} screenId ID of screen to switch to
 */
function setScreen (screenId) {
    for (screen of document.getElementsByClassName("screen")) {
        screen.style.display = "none"
    }
    document.getElementById(screenId).style.display = "block"
}
window.onload = function () {
    loadSettings()
    loadSensors()
    loadUsers()
    mainHomeStatus()
    if (typeof localStorage.localInstances != "undefined") { // Managed instance
        window.global_state = JSON.parse(localStorage.localInstances)[localStorage.instanceID].globalState
    } else { // Unmanaged instance
        window.global_state = localStorage.globalState // TODO: Set a default if instance is unmanaged and no global state is set
    }
    armState(global_state)
    // set "last updated"
    let date = new Date()
    document.getElementById("date").innerText = date.getMonth() + "/" + date.getDay() + "/" + date.getFullYear()
}
/**
 * Converts number of seconds to a human readable timestring (eg. 60 returns 1:00)
 * @param {number} seconds
 * @returns {string} timestring
 */
function toTimestring (seconds) {
    let timestring = ""
    if (seconds >= 60) {
        timestring = timestring + Math.floor(seconds / 60) + ":"
    }
    timestring = timestring + (seconds % 60).toString().padStart(2, "0")
    return timestring
}
/**
 * MUST be called when changing the arm state of the system
 * @param {string} type arm state
 */
function onArm (type) {
    keypad.input = ""
    window.settings["exit-delay"]
    document.getElementById("arm-countdown").innerText = toTimestring(countdown)
    document.getElementById("cancel-timer").innerText = toTimestring(countdown)
    document.getElementById("cancel-screen").getElementsByTagName("h1")[0].innerText = "System will arm in"
    window.countdown_clock = setInterval(function () {
        if (countdown == 0) {
            armState(type)
        } else {
            countdown = countdown - 1
            document.getElementById("arm-countdown").innerText = toTimestring(countdown)
            document.getElementById("cancel-timer").innerText = toTimestring(countdown)
        }
    }, 1000)
}
/**
 * Sets arm state of system
 * @param {string} state State to enter
 */
function armState (state) {
    window.countdown = settings["exit-delay"]
    if (typeof countdown_clock != "undefined") {
        clearInterval(countdown_clock)
    }
    global_state = state
    if (typeof localStorage.instanceID == "undefined") { // Unmanaged instance
        setLocalStorageItem("globalState", state)
    } else { // Managed instance
        syncManagedInstance()
    }
    switch (state) {
        case "disarmed":
            setScreen("main-home")
            parent.document.getElementById("panel").src = "alder simple.webp"
            break
        case "armed-away":
            setScreen("armed")
            document.getElementById("armed-state-label").innerText = "Away"
            showKeypad("darkred", function () {armState("disarmed")}, true)
            parent.document.getElementById("panel").src = "alder simple armed.webp"
            break 
        case "armed-stay":
            document.getElementById("armed-state-label").innerText = "Stay"
            setScreen("armed")
            showKeypad("darkred", function () {armState("disarmed")}, true)
            parent.document.getElementById("panel").src = "alder simple armed.webp"
            break
        case "arming-stay":
            setScreen("arm")
            onArm("armed-stay")
            document.getElementById("arming-state-label").innerText = "Stay"
            document.getElementById("stay-arm-buttons").style.display = "block"
            document.getElementById("away-arm-buttons").style.display = "none"
            break
        case "arming-away":
            setScreen("arm")
            onArm("armed-away")
            document.getElementById("arming-state-label").innerText = "Away"
            document.getElementById("stay-arm-buttons").style.display = "none"
            document.getElementById("away-arm-buttons").style.display = "block"
            break
        case "alarm":
            alarm("", "") // This is so if the panel is rebooted during an alarm, we will go back to the alarm screen. Calling an alarm with no type and no trigger seems jank, but I assure you this is how the real panel handles this
            break
        default:
            console.log("Cannot switch to non existent state " + state)
            break
    }
}
// keypad
window.keypadInput = ""
/**
 * Inputs into the keypad. Can input a number, a backspace, or clear the input
 * @param {(number|"bk"|"clr")} input key to press. Must be a 1 digit number, "bk", or "clr"
 */
function keypadType (input) { 
    if ([1,2,3,4,5,6,7,8,9,0].includes(parseInt(input, 10))) {
        keypadInput = keypadInput + input
        document.getElementById("keypadInput").innerText = document.getElementById("keypadInput").innerText + "*"
    } 
    else if (input == "bk") {
        document.getElementById("keypadInput").innerText = document.getElementById("keypadInput").innerText.slice(0, -1)
        keypadInput = keypadInput.slice(0, -1)
    } else if (input == "clr") {
        document.getElementById("keypadInput").innerText = ""
        keypadInput = ""
    } else {
        console.log("Invalid input " + input)
    }
    if (keypadInput.length > 3) {
        if (verifyCode) {
            if (checkCode(masterOnly)) {
                keypad_callback(keypadInput)
                hideKeypad()
            } else {
                keypadType("clr")
            } 
        } else {
            keypad_callback(keypadInput)
        }
    }
}
/**
 * Show the keypad
 * @param {string} color Color of underline for keypad buttons
 * @param {function} callback Function to be run upon the correct code being entered
 * @param {boolean=false} verifyCode If set to true, callback will only be run if code is correct, and keypad will be hidden automatically when callback is run
 * @param {boolean=false} masterOnly If set to true, only the master code will allow the callback to be run. This will be ignored
 */
function showKeypad (color, callback, verifyCode=false, masterOnly=false) {
    window.verifyCode = verifyCode
    window.keypad_callback = callback
    window.masterOnly = masterOnly
    document.getElementById("keypad").style.display = "flex"
    let buttons = document.getElementById("keypad").children
    for (const button of buttons) {
        button.style.borderBottomColor = color
    }
}
/**
 * Hides the keypad
 */
function hideKeypad () {
    document.getElementById("keypad").style.display = "none"
    keypadType('clr') // clear the keypad so next time we show it, it will be blank
}
/**
 * Check if the code entered into the keypad is correct
 * @param {Boolean=false} masterOnly If true, only the master code will be accepted
 * @returns {Boolean}
 */
function checkCode (masterOnly=false) {
    const cached_code = keypadInput
    keypadInput = ""
    document.getElementById("keypadInput").innerText = ""
    if (masterOnly) {
        if (cached_code == users["master"]) {
            return true
        }
    } else {
        for (const user in users) {
            if (users[user] == cached_code) {
                return true
            }
        }
    }
    return false
}
document.onkeydown = function (input) {
    if (document.getElementById("keypad").style.display == "flex") { // virtual keypad is shown, forward real keypad inputs to virtual keypad
        if ([0,1,2,3,4,5,6,7,8,9].includes(parseInt(input.key, 10))) {
            keypadType(input.key)
        }
        if (input.key == "Backspace") {
            keypadType("bk")
        }
        if (input.key == "Delete") {
            keypadType("clr")
        }
    }
    parent.keydownhandler(input)
}
/**
 * Paginate a table. This function is pretty complicated but I'll do my best to document it
 * @param {Object} table The <table> element
 * @param {HTMLCollection} tableRows List of <tr> elements to be paginated 
 * @param {int} page Which page to display
 * @param {Object} prev Previous <button> element 
 * @param {Object} next Next <button> element
 */
function paginateTable(table, tableRows, page, prev, next) {
    // Clear the current contents of table
    const temp_nodes = Array.from(table.getElementsByTagName("tbody")[0].children)
    for (const child of temp_nodes) { // clear the table first
        if (!child.getAttribute("persists")) {
            child.remove()
        }
    }
    const remainingNodes = table.getElementsByTagName("tbody")[0].children.length
    // Set page globally so its easy to change pages later
    window.page = page
    // Figure out if the prebvious and/or next button should be displayed
    if (page == 0) {
        prev.style.visibility = "hidden";
    } else {
        prev.style.visibility = "visible";
    }
    if (page*5+5 >= Object.keys(tableRows).length) {
        next.style.visibility = "hidden";
    } else {
        next.style.visibility = "visible";
    }
    // Actually load the page
    let offset = page * 5
    let i = offset
    while ((i<offset+5) && (Object.keys(tableRows).length > i)) { // Loop until we have reache the offset + 5, or until there are no more items to list
        table.getElementsByTagName("tbody")[0].appendChild(tableRows[i])
        i++
    }
    // Fill in the rest of the page with empty rows
    for (i = i + remainingNodes; i < offset + 5; i++) { 
        table.getElementsByTagName("tbody")[0].appendChild(document.createElement("tr"))
    }
}
/**
 * Show a list of all sensors of a given type on the screen
 * @param {string} displayName Name of sensor type that will be displayed at the top of the sensor list
 * @param {string} type Type of sensor to list
 * @param {Number} page What page to show
 */
function listSensors (displayName=null, type=null) {
    if (displayName != null) { // assume same display name if not specified
        document.getElementById("sensor-list").getElementsByTagName("h1")[0].innerText = displayName
    }
    if (type == null) { // assume same sensor type if not specified
        type = sensorListType
    }
    window.sensorListType = type
    setScreen("sensor-list")
    for (const node of document.getElementById("sensor-activated").getElementsByTagName("span")) { //TODO: Figure out why this is here
        node.innerText = displayName
    }
    document.getElementById("add-sensor-button").onclick = function () {addSensor(type)}
    generateSensorList(type, 0)
 }
/**
 * Finds all sensors of a given type
 * @param {string} type Type of sensor to find
 * @returns {Number[]} List of sensors of given type
 */
function getSensorsByType (type) {
    let results = {}
    for (const sensor in sensors) {
        if (sensors[sensor].type == type) {
            results[sensor] = sensors[sensor]
        }
    }
    return results
}
/**
 * Populate the list of sensors in #list-sensors. You probably shouldn't be calling this directly - use listSensors()
 * @param {String} type Type of sensor to list
 * @param {Number} offset How many sensors to offset the list by. Useful for pagination.
 */
function generateSensorList (type, page) {
    const temp_nodes = Array.from(document.getElementById("sensor-list").getElementsByTagName("tbody")[0].children)
    for (const child of temp_nodes) { // clear the table first
        child.remove()
    }
    window.sensorlist = getSensorsByType(type) // TODO: Why is this global
    let rows = []
    let sensorlist = getSensorsByType(type)
    for (let sensor in sensorlist) {
        let row = document.createElement("tr")
        let column = document.createElement("td")
        column.innerText = sensorlist[sensor]["name"]
        row.appendChild(column)
        column = document.createElement("td")
        if (!["door-contact", "motion"].includes(type)) {
            column.style.visibility = "hidden" // this stays in the flow of the column on the real panel which is why im doing it like this
        }
        column.innerText = sensorlist[sensor].entryDelay + "s (Delay)"
        row.appendChild(column)
        column = document.createElement("td")
        const battery = document.createElement("img")
        battery.src = "assets/battery.webp"
        column.appendChild(battery)
        row.appendChild(column)
        column = document.createElement("td")
        if (sensorlist[sensor].tamper) { //TODO: Use a switch statement for this
            column.innerText = "Tampered"
            column.style.color = "red"
        } else {
            if (sensorlist[sensor].state) {
                column.innerText = "Open"
                column.style.color = "orangered"
            } else {
                column.innerText = "Closed"
                column.style.color = "#00b7ff"
            }
        }
        column.classList.add("sensor-state")
        row.appendChild(column)
        column = document.createElement("td")
        const edit = document.createElement("button")
        edit.innerText = "Edit"
        edit.onclick = function () {sensorMaintainence(this.parentNode.parentNode.id)}
        column.appendChild(edit)
        row.appendChild(column)
        column = document.createElement("td")
        column.innerText = "X"
        column.classList.add("delete-sensor")
        column.classList.add("clickable")
        column.onclick = function () {deleteSensor(this.parentNode.id)}
        row.id = sensor
        row.appendChild(column)
        rows.push(row)
    }
    paginateTable(document.getElementById("sensor-list").getElementsByTagName("table")[0], rows, page, document.getElementById("sensors-previous"),  document.getElementById("sensors-next"))
}
/**
 * Counts down for 30 seconds so you can clean the screen. Not useful in an emulator but still here for accuracy
 */
function cleanScreenCountdown () {
    let clean_screen_countdown_timer = 30
    window.clean_screen_countdown_interval = setInterval(() => {
        if (clean_screen_countdown_timer == 1) {
            clearInterval(clean_screen_countdown_interval)
            setScreen('display')
        } else {
            clean_screen_countdown_timer = clean_screen_countdown_timer - 1
            document.getElementById("clean-screen").getElementsByTagName("span")[0].innerText = clean_screen_countdown_timer
        }
    }, 1000);
}
// settings
/**
 * Loads panel settings from LocalStorage into application memory
 */
let settings
function loadSettings () {
    if (typeof localStorage.instanceID == "undefined") { // Unmanaged instance
        if (typeof localStorage.settings == "undefined") {
            settings = { // Default Settings
                "led_buttons": true,
                "panel-mount": "wall", // Can be: wall, table
                "secure-arm": false,
                "exit-delay": 60, // can be: 45, 60, 75, 90, 105, 120, 135, 150, 165, 180, 195, 210, 225, 240, 255
                "transmission-delay": 0 // can be: 0, 15, 30, 45
            }
        } else {
            settings = JSON.parse(localStorage.settings)
        }
    } else {
        let localInstances = JSON.parse(localStorage.localInstances)
        settings = localInstances[localStorage.instanceID].settings
    }
    if (!settings.led_buttons) { // This will not work properly if CORS is configured improperly between the panel and the emulator, or if the emulator is running outside of the panel
        parent.document.getElementById("panel").style.filter = "grayscale(1)"
        document.getElementById("leds-on").style.backgroundColor = "gray"
        document.getElementById("leds-off").style.backgroundColor = "red"
    } else {
        parent.document.getElementById("panel").style.filter = "grayscale(0)" 
        document.getElementById("leds-on").style.backgroundColor = "blue"
        document.getElementById("leds-off").style.backgroundColor = "gray"
    }
    if (settings["panel-mount"] == "wall") {
        document.getElementById("wall").style.backgroundColor = "blue"
        document.getElementById("table").style.backgroundColor = "gray"
    } else if (settings["panel-mount"] == "table") {
        document.getElementById("wall").style.backgroundColor = "gray"
        document.getElementById("table").style.backgroundColor = "blue"
    } else {
        alert("ERROR: Invalid panel mount \""+ settings["panel-mount"] + "\"")
    }
    if (settings["exit-delay"] % 15 == 0) {
        document.getElementById("exit-delay-duration").innerText = settings["exit-delay"]
    } else {
        alert("Invalid exit delay " + settings["exit-delay"])
    }
}
/**
 * Save panel settings in panel memory to LocalStorage
 */
function saveSettings () {
    if (typeof localStorage.instanceID == "undefined") { // Unamanged instance
        setLocalStorageItem("settings", JSON.stringify(settings))
    } else {
        syncManagedInstance()
    }
    
}
/**
 * Updates a panel setting both in the application memory and LocalStorage, then hot-reloads panel settings
 * @param {string} key 
 * @param {*} value 
 */
function updateSetting (key, value) {
    settings[key] = value
    saveSettings()
    loadSettings()
}

// exit delay
/**
 * Increase the delay before the alarm sets when leaving
 */
function increaseExitDelay () {
    if (settings["exit-delay"] < 255) {
        updateSetting("exit-delay", settings["exit-delay"] + 15)
    }
}
/**
 * Decrease the delay before the alarm sets when leaving
 */
function decreaseExitDelay () {
    if (settings["exit-delay"] > 45) {
        updateSetting("exit-delay", settings["exit-delay"] - 15)
    }
}
window.sensorAddNames = { // These are stored in an Object so they can be easily referenced both for naming and renaming a sensor
    "door-contact": "Name Door Sensor",
    "window-contact": "Name Window Sensor",
    "motion": "Name Motion Sensor",
    "remote": "Name Remote",
    "glass": "Name Glass Sensor",
    "medical": "Name Medical Sensor",
    "flood": "Name Flood",
    "smoke-co": "Name Smoke + CO",
    "shf": "Name Smoke/Heat/Freeze",
    "co": "Name Carbon Monoxide"
}
/**
 * Begins process of adding a sensor to the panel
 * @param {string} type type of sensor to add
 */
function addSensor (type) {
    window.type = type
    document.getElementById("sensor-name-text").innerText = sensorAddNames[type]
    if (type == "door-contact") {
        document.getElementById("add-sensor-button").onclick = function () {
            displayInstructions('Trigger the door<br>sensor by separating<br>the two pieces.', 'assets/add-contact-sensor.webp', '24%')
            generateNameButtons(1, nameSensorCallback, 'add-sensor', 'door-contact')
        }
    } else if (type == "window-contact") {
        document.getElementById("add-sensor-button").onclick = function () {
            displayInstructions('Trigger the window<br>sensor by separating<br>the two pieces.', 'assets/add-contact-sensor.webp', '24%')
            generateNameButtons(1, nameSensorCallback, 'add-sensor', 'window-contact')
        }
    } else if (type == "motion") {
        document.getElementById("add-sensor-button").onclick = function () {setScreen("add-motion-sensor"); generateNameButtons(1, nameSensorCallback, 'add-sensor', 'motion')}
    } else if (type == "remote") {
        document.getElementById("add-sensor-button").onclick = function () {
            displayInstructions('Press and hold any<br>button for two<br>seconds to trigger<br>Key remote.', 'assets/add-remote.webp', '10%')
            generateNameButtons(1, nameSensorCallback, 'add-sensor', 'remote')
        }
    } else if (type == "glass") {
        document.getElementById("add-sensor-button").onclick = function () {
            displayInstructions('Trigger the Glass<br>Break sensor by<br>twisting the detector<br>clounter clockwise, seperate<br>it from the base,<br>reconnect it to the base,<br>then twist clockwise.', 'assets/add-glassbreak.webp', '9%')
            generateNameButtons(1, nameSensorCallback, 'add-sensor', 'glass')
        }
    } else if (type == "medical") {
        document.getElementById("add-sensor-button").onclick = function () {
            displayInstructions('Press and hold the<br>help button for 3<br>seconds to trigger<br>the Pendant.', 'assets/add-medical.webp', '15%')
            generateNameButtons(1, nameSensorCallback, 'add-sensor', 'medical')
        }
    } else if (type == "flood") {
        document.getElementById("add-sensor-button").onclick = function () {
            displayInstructions('Trigger the<br>Flood/Freeze sensor<br>by pressing the test<br>button on the bottom<br>of the device.', 'assets/add-flood.webp', '15%')
            generateNameButtons(1, nameSensorCallback, 'add-sensor', 'flood')
        }
    } else if (type == "smoke-co") {
        document.getElementById("add-sensor-button").onclick = function () {
            displayInstructions('Trigger the<br>Smoke/CO sensor<br>by pressing the learn<br>button.', 'assets/add-smoke-co.webp', '15%')
            generateNameButtons(1, nameSensorCallback, 'add-sensor','smoke-co')
        }
    } else if (type == "shf") {
        document.getElementById("add-sensor-button").onclick = function () {
            displayInstructions('Trigger the<br>Smoke/Heat/Freeze<br>sensor by pressing<br>the test button.', 'assets/add-shf.webp', '15%')
            generateNameButtons(1, nameSensorCallback, 'add-sensor', 'shf')
        }
    } else if (type == "co") {
        document.getElementById("add-sensor-button").onclick = function () {
            displayInstructions('Trigger the<br>Carbon Monoxide<br>sensor by pressing<br>the test button.', 'assets/add-co.webp', '15%')
            generateNameButtons(1, nameSensorCallback, 'add-sensor', 'co')
        }
    } else {
        alert("ERROR: Cannot add sensor of unknown type "+ type)
    }
}
/**
 * Prompts the user for removal of a sensor
 * @param {int} txid the TXID to be removed
 */
function deleteSensor(txid) {
    document.getElementById("forget-sensor").getElementsByTagName("span")[0].innerText = sensors[txid].name
    document.getElementById("confirm-forget-sensor").data = txid
    document.getElementById("confirm-forget-sensor").onclick = function () {
        let type = sensors[this.data].type // Cache this before we delete the sensor so we know where to send the user
        forgetSensor(this.data)
        generateSensorList(type, 0)
        setScreen("sensor-list")
    }
    setScreen("forget-sensor")
}
/**
 * Removes a sensor from the panel. Does not delete the virtual sensor, only unpairs it from the panel
 * @param {number} txid TXID of sensor to be removed
 */
function forgetSensor(txid) {
    delete sensors[txid]
    saveSensors()
}
/**
 * Shows instructions on triggering a sensor to add it to the panel
 * @param {string} instructions Instructions on how to trigger the sensor
 * @param {string} img Illustration of triggering the sensor
 * @param {string} img_size Image size (percent preferred)
 */
function displayInstructions (instructions, img, img_size) {
    document.getElementById("add-sensor").getElementsByTagName("h1")[0].innerHTML = instructions
    document.getElementById("add-sensor").getElementsByTagName("img")[1].src = img
    document.getElementById("add-sensor").getElementsByTagName("img")[1].style.width = img_size
    setScreen("add-sensor")
}
// Hard coded sensor name lists
const door_contact = ["Door 1", "Back Door", "Basement Door", "Bedroom Door", "Den Door", "Driveway Door", "Front Door", "Garage Door", "Kitchen Door", "Laundry Room Door", "Living Room Door", "Master Bed Door", "Patio Door", "Shed Door", "Shop Door", "Side Door", "Sliding Door", "Utility Door"]
const window_contact = ["Window 1", "Back Window", "Basement Window", "Bathroom Window", "Bedroom Window", "Child's Room Window", "Den Window", "Dining Room Window", "Family Room Window", "Front Window", "Garage Window", "Kitchen Window", "Laundry Window", "Living Room Window", "Master Bed Window", "Office Window", "Side Window", "Spare Room Window"]
const motion = ["Motion 1", "Back Motion", "Bedroom Motion", "Den Motion", "Dining Room Motion", "Downstairs Motion", "Family Room Motion", "Front Motion", "Garage Motion", "Hallway Motion", "Living Room Motion", "Master Bed Motion", "Office Motion", "Shop Motion", "Side Motion", "Upstairs Motion"]
const glass = ["Glass 1", "Back Glass", "Basement Glass", "Bedroom Glass", "Den Glass", "Dining Room Glass", "Downstairs Glass", "Family Room Glass", "Front Glass", "Garage Glass", "Hall Glass", "Kitchen Glass", "Living Room Glass", "Master Bedroom Glass", "Office Glass", "Side Glass", "Sunroom Glass", "Upstairs Glass"]
const flood = ['Flood 1', "Basement Flood", "Bathroom Flood", "Bathroom Sink Flood", "Kitchen Flood", "Kitchen Sink Flood", "Laundry Room Flood", "Master Bath Flood", "Shed Flood", "Storage Room Flood", "Utility Room Flood", "Water Heater Flood"]
const smoke = ["Back", "Basement", "Bedroom", " Den", "Downstairs", "Family Room", "Front", "Hallway", "Living Room", "Upstairs"]
/**
 * Fills in the list of names for naming/rename a sensor
 * @param {number} page page number
 * @param {function} callback Function to be called when a name is selected
 * @param {string} backScreen What sreen to return to if the user presses "back"
 * @param {string|undefined} type type of sensor to generate a list for
 */
function generateNameButtons (page, callback=window.callback, backScreen=window.backScreen, type=globalNameButtonsType) { // TODO: handle incrementing names. also make this a switch
    let sensor_names = []
    window.installed_screen = ""
    window.callback = callback
    window.backScreen = backScreen
    window.globalNameButtonsType = type // Store this globally in case we need to paginate later without the type
    if (type == "door-contact") {
        sensor_names = door_contact
        installed_screen = "contact-sensor-installed"
    } else if (type == "window-contact") {
        sensor_names = window_contact
        installed_screen = "contact-sensor-installed"
    } else if (type == "motion") {
        sensor_names = motion
        installed_screen = "motion-sensor-installed"
    } else if (type == "remote") { // Keyfobs do not have an install screen. They skip straight to the sensor activated screen.
        sensor_names = []
        installed_screen = "generic-installed"
        for (let i = 1; i <= 18; i++) {
            sensor_names.push("Remote " + i)
        }
    } else if (type == "glass") {
        sensor_names = glass
        installed_screen = "glass-installed"
    } else if (type == "medical") { // medical sensors do not have an install screen. They skip straight to the sensor activated screen.
        sensor_names = []
        for (i = 1; i <= 18; i++) {
            sensor_names.push("Medical " + i)
        }
    } else if (type == "flood") {
        sensor_names = flood
        installed_screen = "flood-installed"
    } else if (type == "smoke-co") {
        sensor_names = ["Smoke CO 1"]
        installed_screen = "generic-installed"
        for (const sensor_name of smoke) {
            sensor_names.push(sensor_name + " Smoke CO")
        }
    } else if (type == "shf") {
        sensor_names = ["Smoke HF 1"]
        installed_screen = "shf-installed"
        for (sensor_name of smoke) {
            sensor_names.push(sensor_name + " Smoke HF")
        }
    } else if (type == "co") {
        sensor_names = ["CO 1"]
        installed_screen = "generic-installed"
        for (const sensor_name of smoke) {
            sensor_names.push(sensor_name + " CO")
        }
    } else if (type == "usernames") {
        document.getElementById("sensor-name-text").innerText = "Add New User"
        sensor_names = generateUsernames()
    }
    for (let sensor in sensors) { // throw out used names
        if (sensor_names.includes(sensors[sensor].name)) {
            sensor_names.splice(sensor_names.indexOf(sensors[sensor].name), 1)
        }
    }
    document.getElementById("name-buttons").innerHTML = ""
    let buttons = page-1
    for (const sensor_name of sensor_names.slice((page-1)*12, sensor_names.length)) {
        const button = document.createElement("button")
        button.innerText = sensor_name
        button.setAttribute("onclick", "callback(this)")
        document.getElementById("name-buttons").appendChild(button)
        buttons++
        if (buttons >= page*12) {
            break
        }
    }
    if (page == 1) {
        document.getElementById("next-page").style.display = "block"
        document.getElementById("previous-page").style.display = "none"
    } else {
        document.getElementById("previous-page").style.display = "block"
        document.getElementById("next-page").style.display = "none"

    }
    if (sensor_names.length <= 12) {
        document.getElementById("next-page").style.display = "none"
    }
}
function nameSensorCallback(node) {
    document.getElementById(installed_screen).getElementsByTagName("span")[0].innerText = node.innerText
    setScreen(installed_screen)
    sensors[added_sensor['txid']] = {
        "name": node.innerText,
        "state": added_sensor.state,
        "low-bat": added_sensor["low-bat"],
        "tamper": added_sensor["tamper"],
        "supervised": added_sensor.supervised,
        "type": type
    }
    if (type == "door-contact") {
        sensors[added_sensor['txid']].entryDelay = 30
        sensors[added_sensor['txid']].active = true
    }
    if (type == "window-contact") {
        sensors[added_sensor['txid']].active = true
    }
    if (type == "motion") {
        sensors[added_sensor['txid']].entryDelay = 0
        sensors[added_sensor['txid']].activeInStay = false
        sensors[added_sensor['txid']].active = true
    }
    if (type == "remote") {
        sensors[added_sensor['txid']].sos = true
    }
    if (type == "smoke-co") {
        sensors[added_sensor['txid']].alarmVerify = false
    }
    if (type == "shf") {
        sensors[added_sensor['txid']].smoke = true
        sensors[added_sensor['txid']].heat = true
        sensors[added_sensor['txid']].freeze = true
        sensors[added_sensor['txid']].alarmVerify = true
    }
    if (type == "flood") {
        sensors[added_sensor['txid']].flood = true
        sensors[added_sensor['txid']].freeze = true
    }
    if (type == "glass") {
        sensors[added_sensor['txid']].active = true
    }
    saveSensors() // finally, we can bring everything together and save the sensor
}
/**
 * Generates a list of available usernames
 * @returns string[]
 */
function generateUsernames () {
    let names = []
    for (let i = 1; i <= 20; i++) {
        if (!("User " + i in users)) {
            names.push("User " + i)
        }
    }
    return names
}
/**
 * Load sensors from LocalStorage into application memory
 */
window.sensors = {}
function loadSensors () {
    if (typeof localStorage.instanceID == "undefined") { // Unmanaged instance
        if (typeof localStorage.panelSensors == "undefined") {
            sensors = {}
        } else {
            sensors = JSON.parse(localStorage.panelSensors)
        }
    } else { // Managed instance
        let localInstances = JSON.parse(localStorage.localInstances)
        sensors = localInstances[localStorage.instanceID].panelSensors
    }
}
/**
 * Saves sensors in application memory to LocalStorage
 */
function saveSensors () {
    if (typeof localStorage.instanceID == "undefined") { // Unmanaged instance
        setLocalStorageItem("panelSensors", JSON.stringify(sensors))
    } else { // Managed instance
        syncManagedInstance()
    }
}
/**
 * Arm the system, bypassing any sensors that have issues. Prompt the user first like on the real panel
 * @param {string} state must be arming-stay or arming-away
 */
function bypass (state) {
    document.getElementById("accept-bypass").onclick = function () {armState(state)}
    window.bypassList = []
    for (const sensor in sensors) {
        if (sensors[sensor].state) {
            bypassList.push(sensor)
        }
    }
    setScreen("bypass")
}
window.bypassList = []
/**
 * Fills in the X/X ACTIVE markers for each sensor icon 
 * @returns {Object} Totals and Active counts of each sensor
 */
function activeSensorCount() {
    let count = {}
    for (const sensor in sensors) {
        if (!(sensors[sensor].type in count)) {
            count[sensors[sensor].type] = {"active": 0, "total":0}
        }
        count[sensors[sensor].type].total++
        if (sensors[sensor].active) {
            count[sensors[sensor].type].active++
        }
    }
    for (const type in count) {
        document.getElementsByName(type)[0].getElementsByClassName("active-count")[0].innerText = count[type].active + "/" + count[type].total + " ACTIVE"
    }
    return count
}
/**
 * Opens maintainence screen for a sensor
 * @param {Number} sensor The TXID of the sensor to edit
 */
function sensorMaintainence (sensor) {
    document.getElementById("sensor-maintainence-title").getElementsByTagName("h1")[0].innerText = sensors[sensor].name
    document.getElementById("sensor-maintainence").getElementsByTagName("tr")[0].children[1].innerText = sensor
    document.getElementById("sensor-maintainence").name = sensor
    window.globalNameButtonsType = sensors[sensor].type // Set this in case the user renames the sensor
    manageEntryDelay(sensor, document.getElementById("entry-delay-decrease"), document.getElementById("entry-delay-maintainence"), document.getElementById("entry-delay-increase"))
    if (!sensors[sensor].active) {
        document.getElementById("bypass-on").style.backgroundColor = "#019b85"
        document.getElementById("bypass-off").style.backgroundColor = "gray"
    } else {
        document.getElementById("bypass-on").style.backgroundColor = "gray"
        document.getElementById("bypass-off").style.backgroundColor = "red"
    }
    setScreen("sensor-maintainence")
}
/**
 * Activates or bypasses a sensor, then refreshes the sensor maintainence page
 * @param {int} txid TXID of the sensor to activate/bypass
 * @param {bool} state Rather or not the sensor should be active
 */
function sensorActive (txid, state) {
        sensors[txid].active = state
        sensorMaintainence(txid)
}
/**
 * Simulates pressing the physical home button
 */
function homeButton () {
    if (global_state == "disarmed") {
        setScreen("main-home")
        clearInterval(clean_screen_countdown_interval)
        hideKeypad()
    }
}
/**
 * Sets the system to an arming state
 * @param {"arming-stay"|"arming-away"} state arming state to switch to
 */
function arm(state) {
    if (settings["secure-arm"]) { // Secure arm is enabled. Prompt for passcode before changing state
        showKeypad("blue", function (code) {
            armState(state)
            hideKeypad()
        }, true)
        setScreen("secure-arm")
    } else {
        armState(state)
    }
}
// User Management
/**
 * Loads users from LocalStorage into application memory
 */
function loadUsers () {
    if (typeof localStorage.instanceID == "undefined") { // Local Instance
        if (typeof localStorage.users == "undefined") {
            window.users = { // default user config
                "master": 1234,
                "hostage": null
            }
        } else {
            window.users = JSON.parse(localStorage.users)
        }
    } else { // Cloud Instance
        window.users = JSON.parse(localStorage.localInstances)[localStorage.instanceID]["users"]
    }
}
/**
 * Saves users from application memory into LocalStorage
 */
function saveUsers () {
    if (typeof localStorage.instanceID == "undefined") { // Unmanaged instance
        localStorage.users = JSON.stringify(users)
    } else { // Managed instance
        syncManagedInstance()
    }
}
/**
 * Sets a users password
 * @param {string | "master" | "hostage"} user The users whos password you wish to change
 * @param {Number} passcode Password as a 4 digit int
 */
function updateUserPasscode (user, passcode) {
    users[user] = passcode
    saveUsers()
}
function deleteUser (user) {
    document.getElementById("confirm-delete-user").onclick = function () {
        delete users[user]
        saveUsers()
        userOperationSuccess("This user has been<br>deleted successfully.")
    }
    setScreen("delete-user")
}
/**
 * Populates and displays the user management screen
 */
function userManagement () {
    let rows = []
    for (const user in users) { // Exclude master and hostage from the list of users to list
        if (user == "master" || user == "hostage") {
            continue
        }
        // Name
        let row = document.createElement("tr")
        let column = document.createElement("td")
        column.innerText = user + " Passcode"
        row.appendChild(column)
        // Passcode
        column = document.createElement("td")
        column.innerText = users[user]
        row.appendChild(column)
        // reset/delete
        column = document.createElement("td")
        column.innerHTML = "<button style='background-color: blue'>RESET</button><span onclick='deleteUser(this.parentNode.parentNode.id)' class='delete-user clickable'>X</span>"
        row.appendChild(column)
        row.id = user
        rows.push(row)
    }
    paginateTable(document.getElementById("users").getElementsByTagName("table")[0], rows, 0, document.getElementById("users-previous"), document.getElementById("users-next"))
    document.getElementById("master-passcode").innerText = users["master"]
    if (users["hostage"] == null) {
        document.getElementById("hostage-passcode").innerText = "----"
        document.getElementById("create-hostage").style.display = "initial"
        document.getElementById("reset-hostage").style.display = "none"
    } else {
        document.getElementById("hostage-passcode").innerText = users["hostage"]
        document.getElementById("create-hostage").style.display = "none"
        document.getElementById("reset-hostage").style.display = "initial"
    }
    if (settings["secure-arm"]) {
        document.getElementById("secure-arm-on").style.backgroundColor = "blue"
        document.getElementById("secure-arm-off").style.backgroundColor = "gray"
    } else {
        document.getElementById("secure-arm-on").style.backgroundColor = "gray"
        document.getElementById("secure-arm-off").style.backgroundColor = "red"
    }
    setScreen("users")
}
/**
 * Populate and show the "Change Passcode" prompt
 * @param {string | "master" | "hostage"} user The user whos password you wish to change
 */
function updateUserPasscodePrompt(user) {
    if (typeof user == "object") {
        user = user.innerText
    }
    setScreen("update-passcode")
    showKeypad("#2c9fe5", function (code) {
        hideKeypad()
        updateUserPasscode(user, code)
        if (user == "master") {
            userOperationSuccess("Great! Your master<br>passcode has been<br>reset successfully.")
        } else if (user == "hostage") {
            userOperationSuccess("Great! Your hostage<br>passcode has been<br>reset successfully.")
        } else {
            userOperationSuccess("This user has been<br>added successfully.")
        }
    })
}
/**
 * Populates the Success screen for user operations
 * @param {string} message Mesage to show on success screen
 */
function userOperationSuccess(message) {
    document.getElementById("user-management-success").getElementsByTagName("h1")[0].innerHTML = message
    setScreen("user-management-success")
}
// Security monitoring
window.entryDelay = false
/**
 * Sensor event handler for triggering alarm states
 * @param {Object} update Update provided by sensor event
 */
function securityMonitoring(update) {
    const old_value = JSON.parse(update.oldValue)
    const new_value = JSON.parse(update.newValue)
    for (const sensor in old_value) {
        if (!sensors[sensor].active || !sensors[sensor].supervised) { // Ignore bypassed and unsupervised sensors
            continue 
        }
        if (old_value[sensor].state != new_value[sensor].state) { // We found the sensor that was triggered
            // Burglar Alarm
            if ((sensors[sensor].type == "door-contact" || sensors[sensor].type == "window-contact") && global_state == "armed-stay") { // if we are armed away, we should run the entry delay first
                armState("alarm")
                alarm("Burger Alarm", sensors[sensor].name)
            }
            if ((sensors[sensor].type == "door-contact" || sensors[sensor].type == "motion") && global_state == "armed-away") { // Entry Delay. Note that this does not change the arm state. This is because the real panel doesn't either. Rebooting the real panel during an entry delay goes back to the armed screen
                if (!entryDelay) { // Do nothing if there is already an entry delay running
                    window.entryDelay = true
                    window.countdown = sensors[sensor].entryDelay
                    document.getElementById("cancel-screen").getElementsByTagName("h1")[0].innerText = "Alarm will sound in"
                    showKeypad("darkred", function () {
                        entryDelay = false
                        clearInterval(window.countdown_clock)
                        armState("disarmed")
                    }, true, false)
                    document.getElementById("cancel-timer").innerText = toTimestring(sensors[sensor].entryDelay)
                    window.countdown_clock = setInterval(function () {
                        if (countdown == 0) {
                            entryDelay = false
                            clearInterval(window.countdown_clock)
                            armState("alarm")
                            alarm("Burglar Alarm", sensors[sensor].name)
                        } else {
                            countdown--
                            document.getElementById("cancel-timer").innerText = toTimestring(countdown)
                        }
                    }, 1000) 
                    setScreen("cancel-screen")
                }
            }
            // SOS alarm
            if (sensors[sensor].type == "remote" && new_value[sensor].state) {
                armState("alarm")
                alarm("SOS Alarm", sensors[sensor].name)
            }
            // Fire alarm
            if ((sensors[sensor].type == "smoke-co" || sensors[sensor.type] == "shf")  && new_value[sensor].state) { // Fire alarms do not care about arm state
                document.getElementById("emergency-signal-text").innerHTML = "Fire Emergenncy<br>Confirmed...<br>Exit Now!"
                armState("alarm")
                setScreen("emergency-signal") // Fire alarms do not use the standard alarm screen
            }
            // CO alarm
            if ((sensors[sensor].type == "co") && new_value[sensor].state) {
                armState("alarm")
                document.getElementById("emergency-signal-text").innerHTML = "Carbon Monoxide<br>Detected<br>Exit Now!"
                setScreen("emergency-signal") // CO alarms do not use the standard alarm screen
            }
            // Medical alarm
            if (sensors[sensor].type == "medical" && new_value[sensor].state) {
                armState("alarm")
                alarm("Medical Alarm", sensors[sensor].name)
            }
            // Flood alarm
            if (sensors[sensor].type == "flood" && new_value[sensor].state) {
                armState("alarm")
                alarm("Flood Alarm", sensors[sensor].name)
            }
        }
    }
}
/**
 * Triggers an alarm                
 * @param {string} type Type of alarm
 * @param {string} trigger Name of sensor that triggered the alarm
 */
function alarm(type, trigger) {
    document.getElementById("alarm-type").innerText = type
    document.getElementById("alarm-trigger").innerText = trigger
    showKeypad("darkred", abortAlarm, true)
    setScreen("alarm")
}
/**
 * Cancels sending alarm to SecureNet and disarms
 */
function abortAlarm() {
    // TODO: Show cancel screen
    armState("disarmed")
}
function syncManagedInstance() { // This may seem inefficient, but it's actually more efficient then doing each property invidually like an unmanaged instance. This is because to update a Managed Instance, you have to parse and re-stringify the instance dictionary
    if (typeof parent.document.getElementById("overlay").contentWindow.sensors == "undefined") { // TODO: Figure out why this happens
        console.log("sensors in overlay context is undefined! This is a bug")
        return
    }
    if (localStorage.instanceID != "undefined") { // Make sure we are actually running in a managed instance
        let localInstances = JSON.parse(localStorage.localInstances)
        // Sync settings
        localInstances[localStorage.instanceID].settings = settings
        // Sync sensors
        localInstances[localStorage.instanceID].sensors = parent.document.getElementById("overlay").contentWindow.sensors
        // Sync panel sensors
        localInstances[localStorage.instanceID].panelSensors = sensors
        // Sync global state
        localInstances[localStorage.instanceID].globalState = global_state
        // TODO: sync users
        localInstances[localStorage.instanceID].users = users
        setLocalStorageItem("localInstances", JSON.stringify(localInstances))
    }
}
/**
 * Sets an item in localStorage and ensures the storage event is fired, even if called from the same context as the update
 * @param {string} key key to be set
 * @param {value} value value to set
 */
function setLocalStorageItem (key, value) {
    const oldValue = localStorage[key]
    localStorage.setItem(key, value)
    const update = {
        "oldValue": oldValue,
        "newValue": value,
        "key": key
    }
    onstorage(update)
}
/**
 * Prompts user to rename a sensor
 * @param {Number} txid TXID of the sensor to prompt for renaming
 */
function renameSensor (txid) {
    document.getElementById("sensor-name-text").innerText = sensorAddNames[globalNameButtonsType]
    generateNameButtons(1, function (name) {
        name = name.innerText
        sensors[txid].name = name
        document.getElementById("sensor-maintainence-title").getElementsByTagName("h1")[0].innerText = name // Set this in case the user returns to the sensor settings
        saveSensors()
        setScreen("rename-success")
    }, 'sensor-maintainence', globalNameButtonsType)
    setScreen('sensor-name')
}
/**
 * Manages "Entry Delay" controls in sensor maintainence. To be called when maintainence screen is opened on a sensor
 * @param {Number} sensor TXID of the sensor to adjust entry delay on
 * @param {Object} decrease Decrease button <span>
 * @param {Object} value <span> containing the entry delay value
 * @param {Object} increase Increase button <span> 
 */
function manageEntryDelay(sensor, decrease, value, increase) {
    value.innerText = sensors[sensor].entryDelay
    decrease.onclick = function () {
        if (sensors[sensor].entryDelay == 0) { // Cannot decrease any further
            return
        }
        if (sensors[sensor].entryDelay == 30) { // For some reason you cannot set the entry delay to 15. It skips from 30 to 0. In all other cases it goes in incriments of 15
            sensors[sensor].entryDelay = 0
        } else {
            sensors[sensor].entryDelay = sensors[sensor].entryDelay - 15
        }
        saveSensors()
        sensorMaintainence(sensor)
    } 
    increase.onclick = function () {
        if (sensors[sensor].entryDelay == 255) { // Cannot increase any further
            return
        }
        if (sensors[sensor].entryDelay == 0) { // For some reason you cannot set the entry delay to 15. It skips from 0 to 30. In all other cases it goes in incriments of 15
            sensors[sensor].entryDelay = 30
        } else {
            sensors[sensor].entryDelay = sensors[sensor].entryDelay + 15
        }
        saveSensors()
        sensorMaintainence(sensor)
    }
}