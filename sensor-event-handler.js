"use strict"
function onstorage (originalUpdate) {
    originalUpdate.oldValue = JSON.parse(originalUpdate.oldValue)
    originalUpdate.newValue = JSON.parse(originalUpdate.newValue)
    let update = {}
    if (originalUpdate.key == "localInstances") {// Emulate an update for Managed instances
        originalUpdate.oldValue = originalUpdate.oldValue[localStorage.instanceID],
        originalUpdate.newValue = originalUpdate.newValue[localStorage.instanceID]
        for (const key in originalUpdate.oldValue) {
            if (JSON.stringify(originalUpdate.oldValue[key]) != JSON.stringify(originalUpdate.newValue[key])) {
                update.key = key
                break
            } 
        }
    } else {
        update.key = key
    }
    update.oldValue = JSON.stringify(originalUpdate.oldValue[update.key])
    update.newValue = JSON.stringify(originalUpdate.newValue[update.key])
    if (update.key == "sensors") {
        detectNewSensor(update)
        sensorSync(update)
        mainHomeStatus(update)
        sensorListStatus(update)
    }
}
window.addEventListener("storage", onstorage)
/**
 * Detect newly added sensors if on #add-sensor screen
 * @param {Object} update Update object from storage event
 */
function detectNewSensor (update) {
    if (document.getElementById("add-motion-sensor").style.display == "block" || document.getElementById("add-sensor").style.display == "block") {
        const old_value = JSON.parse(update.oldValue)
        const new_value = JSON.parse(update.newValue)
        for (const sensor in old_value) { 
            if (old_value[sensor].state != new_value[sensor].state) { // we found the updated sensor
                new_value[sensor]["txid"] = sensor
                window.added_sensor = new_value[sensor]
                document.getElementById("success-txid").innerText = sensor
                setScreen("sensor-added-success")
                break
            }
        }
    }
}
/**
 * Syncs sensor states with panel
 * @param {Object} update Update object from storage event
 */
function sensorSync (update) {
    if (update.newValue == undefined) { // TODO: Figure out why this happens
        return
    }
    const old = sensors
    const new_value = JSON.parse(update.newValue)
    for (const sensor in new_value) {
        if (sensor in sensors) { // Ignore sensors that are not added to the panel
            if (new_value[sensor].supervised && sensors[sensor].active) { // Ignore sensors that are unsupervised or bypassed
                securityMonitoring(update)
                sensors[sensor].supervised = true
                sensors[sensor].state = new_value[sensor].state
                sensors[sensor]["low-bat"] = new_value[sensor]["low-bat"]
                sensors[sensor].tamper = new_value[sensor].tamper
            } else { // don't report anything to the panel if supervised is off, other then supervised is off. This takes forever to go thru irl but can be instant in the simulator
                sensors[sensor].supervised = false
            }
        }
    }
    saveSensors()
}
/**
 * Updates the #main-home status message/buttons
 */
function mainHomeStatus () {
    let openSensors = []
    let tamperedSensors = []
    for (const sensor in sensors) {
        if (sensors[sensor].state) {
            openSensors.push(sensor)    
        }
    }
    for (const sensor in sensors) {
        if (sensors[sensor].tamper) {
            tamperedSensors.push(sensor)
        }
    }
    let totalIssues = openSensors.length + tamperedSensors.length
    if (totalIssues == 0) { //TODO: A switch statement would be good here..
        document.getElementById("home-message").innerHTML = "Hey, <br>I'm ready <br>to arm."
        document.getElementById("home-message").style.fontSize = "20vh"
        document.getElementById("home-message").style.paddingTop = "0"
        document.getElementById("main-home").style.backgroundColor = "#4dbdec"
        document.getElementById("main-home-buttons").style.display = "block"
        document.getElementById("not-ready-buttons").style.display = "none"
    }
    if (openSensors.length == 1) {
        document.getElementById("home-message").innerHTML = "Hang on, your<br>" + sensors[openSensors[0]].name + "<br>is open."
    }
    if (tamperedSensors.length == 1) {
        document.getElementById("home-message").innerHTML = "Hang on, your<br>" + sensors[tamperedSensors[0]].name + "<br>is tampered."
    }
    if (totalIssues > 1) { // Suprisingly, this is accurate to how the panel handles this. If you have more then 1 issue, no matter what the issues are, even if its 2 tampers, it will just say multiple sensors are open.
        document.getElementById("home-message").innerHTML = "Hang on, you have<br>multiple sensors<br>that are open."
    }
    if (totalIssues > 0) {
        document.getElementById("home-message").style.fontSize = "12vh"
        document.getElementById("home-message").style.paddingTop = "5%"
        document.getElementById("main-home").style.backgroundColor = "darkorange"
        document.getElementById("main-home-buttons").style.display = "none"
        document.getElementById("not-ready-buttons").style.display = "block"
    }
}
/**
 * Shows sensor status in sensor list
 * @param {Object} update Update object from storage event
 */
function sensorListStatus (update) {
    if (document.getElementById("sensor-list").style.display == "block") {
        const matchingSensors = getSensorsByType(sensorListType)
        for (const sensor in matchingSensors) {
            if (sensors[sensor].tamper) { // TODO: Use a switch statement for this
                document.getElementById(sensor).getElementsByClassName("sensor-state")[0].innerText = "Tampered"
                document.getElementById(sensor).getElementsByClassName("sensor-state")[0].style.color = "red"
            } else {
                if (sensors[sensor].state) {
                    document.getElementById(sensor).getElementsByClassName("sensor-state")[0].innerText = "Open"
                    document.getElementById(sensor).getElementsByClassName("sensor-state")[0].style.color = "orangered"
                } else {
                    document.getElementById(sensor).getElementsByClassName("sensor-state")[0].innerText = "Closed"
                    document.getElementById(sensor).getElementsByClassName("sensor-state")[0].style.color = "#00b7ff"
                }
            }
        }
    }
}