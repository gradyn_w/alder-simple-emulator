from flask import Flask, send_file
app = Flask(__name__)

print("This is not at all secure so don't use this in prod")
@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def root(path):
    return send_file(path)
app.run('0.0.0.0')
